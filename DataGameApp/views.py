from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.views import View

from .scrapping import *


class RootView(View):
    def get(self, request):
        games = Game.objects.all()
        return render(request, 'home.html', context={'games': games})


class GetNewGamesView(View):
    def get(self, request):
        scrape_new_games()
        games = Game.objects.all()
        return render(request, 'home.html', context={'games': games, 'updated': True})


class GameView(View):
    def get(self, request, game_id):
        try:
            game = Game.objects.get(pk=game_id)
        except Game.DoesNotExist:
            raise Http404("This game does not exist !")
        scrape_new_games()
        scrape_game_critic_reviews(game)
        scrape_game_user_reviews(game)

        order = request.GET.get('order', '-score')
        if order not in ('-score', 'score', '-date', 'date'):
            order = '-score'
        critic_reviews = CriticReview.objects.filter(game=game).order_by(order).all()
        user_reviews = UserReview.objects.filter(game=game).order_by(order).all()
        return render(request, 'game.html', context={'game': game, 'critic_reviews': critic_reviews,
                                                     'user_reviews': user_reviews, 'order': order})


class NewspaperView(View):
    def get(self, request, newspaper_id):
        try:
            newspaper = Newspaper.objects.get(pk=newspaper_id)
        except Newspaper.DoesNotExist:
            raise Http404("This newspaper does not exist !")
        reviews = CriticReview.objects.filter(newspaper=newspaper).all()
        return render(request, 'newspaper_user.html',
                      context={'newspaper_user': newspaper, 'reviews': reviews, 'is_newspaper': True})


class UserView(View):
    def get(self, request, user_id):
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            raise Http404("This newspaper does not exist !")
        reviews = UserReview.objects.filter(user=user).all()
        return render(request, 'newspaper_user.html',
                      context={'newspaper_user': user, 'reviews': reviews, 'is_newspaper': False})
