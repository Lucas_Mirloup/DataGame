from django.db import models


class Game(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    display_name = models.CharField(max_length=255, unique=True)
    platform = models.CharField(max_length=20)
    release_date = models.DateField()
    cover_image_url = models.URLField()
    critics_score = models.SmallIntegerField(default=-1)
    users_score = models.SmallIntegerField(default=-1)
    summary = models.TextField()


class Newspaper(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    display_name = models.CharField(max_length=255, unique=True)


class User(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    display_name = models.CharField(max_length=255, unique=True)


class Review(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    date = models.DateField()
    score = models.SmallIntegerField(default=-1)
    comment = models.TextField()

    class Meta:
        abstract = True


class CriticReview(Review):
    newspaper = models.ForeignKey(Newspaper, on_delete=models.CASCADE)
    article_link = models.CharField(max_length=255)


class UserReview(Review):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
