from datetime import datetime

import bs4
import requests

from DataGameApp.models import CriticReview, Game, Newspaper, User, UserReview

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0'}


def scrape_new_games():
    req = requests.get('https://www.metacritic.com/browse/games/release-date/new-releases/all/date', headers=headers)
    soup = bs4.BeautifulSoup(req.text, 'lxml')
    games_data = soup.select('.clamp-list > tr')
    # games = [x.find_all('tr', recursive=False) for x in soup.find_all('table', {'class': 'clamp-list'})]
    for game_data in games_data:  # type: bs4.Tag
        if 'class' not in game_data.attrs:  # Doesn't parse spacers
            game_id = game_data.select_one('.title').attrs['href'].split('/')[-1]
            critics_score = int(game_data.select_one('.metascore_w').text)
            users_score = game_data.select_one('.metascore_w.user').text
            users_score = int(10 * float(users_score)) if users_score != 'tbd' else -1
            try:
                game = Game.objects.get(pk=game_id)
                game.critics_score = critics_score
                game.users_score = users_score
            except Game.DoesNotExist:
                Game.objects.create(
                    id=game_id,
                    display_name=game_data.select_one('.title > h3').text,
                    platform=game_data.select_one('.data').text.replace('\n', '').strip(),
                    release_date=datetime.strptime(game_data.select_one('.clamp-details > span').text,
                                                   '%B %d, %Y').date(),
                    cover_image_url=game_data.select_one('.clamp-image-wrap > a > img').attrs['src'],
                    critics_score=critics_score,
                    users_score=users_score,
                    summary=game_data.select_one('.summary').text.replace('\r\n', ' ').translate(
                        str.maketrans({'\r': None, '\n': None})).replace('  ', ' ').strip())


def scrape_game_critic_reviews(game: Game):
    platform = game.platform.lower().replace(' ', '-')
    req = requests.get(f'https://www.metacritic.com/game/{platform}/{game.id}/critic-reviews', headers=headers)
    soup = bs4.BeautifulSoup(req.text, 'lxml')
    reviews = soup.select('.critic_review')
    for review in reviews:  # type:bs4.Tag
        newspaper_id = review.select_one('.author_reviews > a').attrs['href'].split('/')[2].split('?')[0]
        try:
            newspaper = Newspaper.objects.get(pk=newspaper_id)
        except Newspaper.DoesNotExist:
            display_name = review.select_one('.source > a')
            display_name = display_name.text if display_name else review.select_one('.source').text
            newspaper = Newspaper.objects.create(id=newspaper_id, display_name=display_name)
        score = review.select_one('.metascore_w').text
        score = int(score) if score.isdecimal() else -1
        comment = review.select_one('.review_body').text.strip()
        article_link = review.select_one('.full_review a')
        article_link = article_link.attrs['href'] if article_link else ""
        try:
            critic_review = CriticReview.objects.get(game=game, newspaper=newspaper)
            critic_review.score = score
            critic_review.comment = comment
            critic_review.article_link = article_link
        except CriticReview.DoesNotExist:
            CriticReview.objects.create(
                game=game,
                date=datetime.strptime(review.select_one('.date').text, '%b %d, %Y').date(),
                score=score,
                comment=comment,
                newspaper=newspaper,
                article_link=article_link)


def scrape_game_user_reviews(game: Game):
    platform = game.platform.lower().replace(' ', '-')
    req = requests.get(f'https://www.metacritic.com/game/{platform}/{game.id}/user-reviews', headers=headers)
    soup = bs4.BeautifulSoup(req.text, 'lxml')
    reviews = soup.select('.user_review')
    for review in reviews:  # type:bs4.Tag
        user_id = review.select_one('.name > a').attrs['href'].split('/')[2]
        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            display_name = review.select_one('.name > a').text
            user = User.objects.create(id=user_id, display_name=display_name)
        score = int(review.select_one('.metascore_w').text) * 10
        comment = review.select_one('.review_body > span').text.strip()
        try:
            user_review = UserReview.objects.get(game=game, user=user)
            user_review.score = score
            user_review.comment = comment
        except UserReview.DoesNotExist:
            UserReview.objects.create(
                game=game,
                date=datetime.strptime(review.select_one('.date').text, '%b %d, %Y').date(),
                score=score,
                comment=comment,
                user=user)
