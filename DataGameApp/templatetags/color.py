from django import template

register = template.Library()


@register.filter()
def color(score: int):
    return "success" if score >= 90 else "primary" if score >= 75 else "warning" if score >= 50 \
        else "danger" if score >= 0 else "dark"
