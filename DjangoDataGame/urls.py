"""DjangoDataGame URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from DataGameApp.views import RootView, GetNewGamesView, GameView, NewspaperView, UserView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RootView.as_view(), name='home'),
    path('update/', GetNewGamesView.as_view(), name='update'),
    path('game/<str:game_id>', GameView.as_view(), name='game'),
    path('newspaper/<str:newspaper_id>', NewspaperView.as_view(), name='newspaper'),
    path('user/<str:user_id>', UserView.as_view(), name='user')
]
